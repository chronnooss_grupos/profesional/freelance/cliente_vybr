/* NAVIGATION*/
// COLLAPSE THE NAVBAR BY ADDING THE TOP-NAV-COLLAPSE CLASS
window.onscroll = function () {
   scrollFunction();
   scrollFunctionBTT(); // back to top button
};

function scrollFunction() {
   let intViewportWidth = window.innerWidth;
   if (document.body.scrollTop > 30 || (document.documentElement.scrollTop > 30) & (intViewportWidth > 991)) {
      document.getElementById("navbar").classList.add("top-nav-collapse");
   } else if (document.body.scrollTop < 30 || (document.documentElement.scrollTop < 30) & (intViewportWidth > 991)) {
      document.getElementById("navbar").classList.remove("top-nav-collapse");
   }
}

// NAVBAR ON MOBILE
let elements = document.querySelectorAll(".nav-link:not(.dropdown-toggle)");

for (let i = 0; i < elements.length; i++) {
   elements[i].addEventListener("click", () => {
      document.querySelector(".offcanvas-collapse").classList.toggle("open");
   });
}

document.querySelector(".navbar-toggler").addEventListener("click", () => {
   document.querySelector(".offcanvas-collapse").classList.toggle("open");
});

/* BACK TO TOP BUTTON */
// GET THE BUTTON
myButton = document.getElementById("myBtn");

// WHEN THE USER SCROLLS DOWN 20PX FROM THE TOP OF THE DOCUMENT, SHOW THE BUTTON
function scrollFunctionBTT() {
   if (document.body.scrollTop > 20 || document.documentElement.scrollTop > 20) {
      myButton.style.display = "block";
   } else {
      myButton.style.display = "none";
   }
}

// WHEN THE USER CLICKS ON THE BUTTON, SCROLL TO THE TOP OF THE DOCUMENT
function topFunction() {
   document.body.scrollTop = 0; // for Safari
   document.documentElement.scrollTop = 0; // for Chrome, Firefox, IE and Opera
}

// AOS ANIMATION ON SCROLL
AOS.init({
   duration: 1000,
   easing: "ease",
   once: true, // whether animation should happen only once - while scrolling down
});
