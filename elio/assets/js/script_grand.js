(function ($) {
   $(document).ready(function () {
      $("body").addClass("js");
      var $menu = $("#menu"),
         $menulink = $(".menu-link");

      $menulink.click(function () {
         $menulink.toggleClass("active");
         $menu.toggleClass("active");
         return false;
      });
   });

   $("div.features-post").hover(
      function () {
         $(this).find("div.content-hide").slideToggle("medium");
      },
      function () {
         $(this).find("div.content-hide").slideToggle("medium");
      }
   );
})(jQuery);
